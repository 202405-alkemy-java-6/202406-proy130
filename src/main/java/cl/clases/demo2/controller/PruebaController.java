package cl.clases.demo2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import cl.clases.demo2.dao.UsuarioDao;
import cl.clases.demo2.entity.Usuario;


@Controller
@RequestMapping("/prueba")
public class PruebaController {

	@Autowired
	private UsuarioDao dao;
	
	@GetMapping("/test")
	private ResponseEntity<String> test(){
		
		Usuario user = new Usuario();
		user.setNombre("FELIPE");
		
		dao.nuevoUsuario(user);
		
		return new ResponseEntity<String>("OK", HttpStatus.OK);
		
	}
	
}
