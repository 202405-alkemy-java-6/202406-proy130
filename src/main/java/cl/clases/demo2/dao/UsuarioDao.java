package cl.clases.demo2.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import cl.clases.demo2.entity.Usuario;

@Repository
public class UsuarioDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public void nuevoUsuario(Usuario usuario) {
		String sql = "INSERT INTO usuario (nombre) VALUES (?)";
		jdbcTemplate.update(sql,usuario.getNombre());
	}
	
}
